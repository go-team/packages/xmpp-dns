// Copyright Martin Dosch.
// Use of this source code is governed by the BSD-2-clause
// license that can be found in the LICENSE file.

package main

import (
	"slices"
	"strings"
)

func isKnownLinkRel(s string) bool {
	// https://www.iana.org/assignments/link-relations/link-relations.xhtml
	// as of 2024-10-07
	linkRels := []string{"about", "acl", "alternate", "amphtml", "appendix", "apple-touch-icon", "apple-touch-startup-image", "archives", "author", "blocked-by", "bookmark", "c2pa-manifest", "canonical", "chapter", "cite-as", "collection", "compression-dictionary", "contents", "convertedfrom", "copyright", "create-form", "current", "deprecation", "describedby", "describes", "disclosure", "dns-prefetch", "duplicate", "edit", "edit-form", "edit-media", "enclosure", "external", "first", "glossary", "help", "hosts", "hub", "ice-server", "icon", "index", "intervalafter", "intervalbefore", "intervalcontains", "intervaldisjoint", "intervalduring", "intervalequals", "intervalfinishedby", "intervalfinishes", "intervalin", "intervalmeets", "intervalmetby", "intervaloverlappedby", "intervaloverlaps", "intervalstartedby", "intervalstarts", "item", "last", "latest-version", "license", "linkset", "lrdd", "manifest", "mask-icon", "me", "media-feed", "memento", "micropub", "modulepreload", "monitor", "monitor-group", "next", "next-archive", "nofollow", "noopener", "noreferrer", "opener", "openid2.local_id", "openid2.provider", "original", "p3pv1", "payment", "pingback", "preconnect", "predecessor-version", "prefetch", "preload", "prerender", "prev", "preview", "previous", "prev-archive", "privacy-policy", "profile", "publication", "related", "restconf", "replies", "ruleinput", "search", "section", "self", "service", "service-desc", "service-doc", "service-meta", "sip-trunking-capability", "sponsored", "start", "status", "stylesheet", "subsection", "successor-version", "sunset", "tag", "terms-of-service", "timegate", "timemap", "type", "ugc", "up", "version-history", "via", "webmention", "working-copy", "working-copy-of"}
	return slices.Contains(linkRels, strings.ToLower(s))
}
